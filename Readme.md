[//]: # (Header)
  
<div align="center">
  <h1>WakeUpAzureWebApp</h1>
    <p>
        <a href="https://gitlab.com/Valtech-Amsterdam/PowerShell/WakeUpAzureWebApp">
            <img height="250" width="250" src="https://gitlab.com/Valtech-Amsterdam/PowerShell/WakeUpAzureWebApp/raw/main/Resource/logo.png"
                alt="WakeUpAzureWebApp">
        </a>
    </p>
</div>
  
[![PowerShell version][PowerShellGallery-version-image]][PowerShellGallery-url]
[![Downloads][PowerShellGallery-downloads-image]][PowerShellGallery-url] 
[![License][license-image]][license-url]
  
[//]: # (Documentation)
## What is WakeUpAzureWebApp?  
  
This is a module dedicated to waking up an Azure WebApp, targeting every instance with a list of relative urls.  
  
## Prerequisites  
  
This module is designed for use in Azure context, so Start-WakeUpAzureWebApp needs to be called before executing the sript.  
This is designed with Octopus deploy in mind.  
  
## Installation  
  
```powershell
Install-Module WakeUpAzureWebApp -Scope CurrentUser
```
  
## Usage  
  
```powershell
Import-Module WakeUpAzureWebApp;
Set-AzContext -SubscriptionId #"{SubscriptionId}"; # This line can be omitted if you have one subscription

$targetWebAppName = #"{WebAppName}";
$warmupUrls = @(
    "/nl-nl"
    "/nl-nl/some url/"
    "/nl-nl/another url/"

    # Absolute urls are allowed, this will ignore the webapp hostname and use the absolute hostname
    # This will only work for "http(s)://"
    "https://www.someAbsoluteDomain.com/WhatEver"
);

Start-WakeUpAzureWebApp `
 -targetWebAppName $targetWebAppName `
 -warmupUrls $warmupUrls `
 -UseExitInsteadOfThrow;
```
If you want to see an example you can check "[./_Example.ps1](https://gitlab.com/Valtech-Amsterdam/PowerShell/WakeUpAzureWebApp/blob/main/_Example.ps1)".
  
### Additional parameters  
  
There are a few optional parameters:  
  
* **RequestTimeOut** (Default = null)  
This parameter requires System.TimeSpan. it defines the maximum amount of time allowed for every web request to respond within.  
When defined this will terminate script on timeout.  
`$timeOut = New-TimeSpan -Seconds 20;`  
`$timeOut = $null;`  
* **NoResponseRetries** (Default = 3)  
This parameter requires a positive integer larger than 0, it defines the amount of times to retry on a non-status code response or timeout.  
* **ConsecutiveErrorThreshold** (Default = 10 Errors)  
This parameter requires a positive integer larger than 0,  
it defines the amount of request errors that are allowed to appear in series before terminating.  
* **TotalErrorThreshold** (Default = No maximum)  
This parameter requires a positive integer larger than 0 but is optional,  
it defines the amount of request errors that are allowed to appear in total before terminating.  
* **UseExitInsteadOfThrow** (Flag)  
If this flag is enabled the error won't be trown but exit(1) will be used.
This makes the process terminate but won't print out the stacktrace.  
* **OutputHtml** (Flag)  
Output response HTML.  
  
### Output
  
**Note:** The screenshots are still saying AzureRM but the library has been upgraded to AzureAZ!  
  
If you're not logged in, for example if you're running on your local machine, the script will terminate telling you to log in:  
![](https://gitlab.com/Valtech-Amsterdam/PowerShell/WakeUpAzureWebApp/raw/main/Resource/no-user.png)  
  
If you're powershell runner supports progress you will see a progress bar indicating progress amongst instances with a child bar 
indicating which url from the argument list you are.  
![](https://gitlab.com/Valtech-Amsterdam/PowerShell/WakeUpAzureWebApp/raw/main/Resource/job-progress.png)  
  
The script will also output a detailed list of the requests with some color coding indicationg whether or not the response was succesfull:  
![](https://gitlab.com/Valtech-Amsterdam/PowerShell/WakeUpAzureWebApp/raw/main/Resource/full-job-output.png)  
  
## [Contributing][contributing-url]
[contributing-url]: https://gitlab.com/Valtech-Amsterdam/PowerShell/WakeUpAzureWebApp/blob/main/Contributing.md
See the [Contribution guide][contributing-url] for help about contributing to this project.  
  
## [Changelog][changelog-url]
[changelog-url]: https://gitlab.com/Valtech-Amsterdam/PowerShell/WakeUpAzureWebApp/blob/main/Changelog.md  
See the [Changelog][changelog-url] to see the change history.  
  
[//]: # (Labels)

[PowerShellGallery-url]: https://www.powershellgallery.com/packages/WakeUpAzureWebApp
[PowerShellGallery-downloads-image]: https://img.shields.io/powershellgallery/dt/WakeUpAzureWebApp.svg?label=downloads&colorB=brightgreen
[PowerShellGallery-version-image]: https://img.shields.io/powershellgallery/v/WakeUpAzureWebApp.svg?label=version
[license-url]: https://gitlab.com/Valtech-Amsterdam/PowerShell/WakeUpAzureWebApp/blob/main/License.md#blob-content-holder
[license-image]: https://img.shields.io/badge/license-Apache--2.0-blue.svg
[build-url]: https://gitlab.com/Valtech-Amsterdam/PowerShell/WakeUpAzureWebApp/pipelines
[build-image]: https://gitlab.com/Valtech-Amsterdam/PowerShell/WakeUpAzureWebApp/badges/main/build.svg