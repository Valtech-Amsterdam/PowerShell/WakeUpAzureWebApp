# Actually push package
$manifest = Get-Content "./WakeUpAzureWebApp.psd1";
$manifest = $manifest.replace("#{ModuleVersion}", "$env:CI_BUILD_TAG");
$manifest | Set-Content "./WakeUpAzureWebApp.psd1"

[Net.ServicePointManager]::SecurityProtocol = [Net.SecurityProtocolType]::Tls12

Write-Host "Publishing module";
Publish-Module `
    -Path "./" `
    -Force `
    -NuGetApiKey "$env:NuGetApiKey";
