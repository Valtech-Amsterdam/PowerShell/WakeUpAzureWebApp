$OriginalPref = $ProgressPreference # Default is 'Continue'
$ProgressPreference = "SilentlyContinue"

if (-not (Get-Module -Name "Az" -ListAvailable)) {
    Write-Host "  Installing AzureAz";
    Install-Module Az `
        -AllowClobber `
        -Scope CurrentUser `
        -Force `
        -SkipPublisherCheck;
}

if (-not (Get-Module -Name "Pester" -ListAvailable)) {
    Write-Host "  Installing Pester";
    Install-Module Pester `
        -Scope CurrentUser `
        -Force `
        -SkipPublisherCheck; 
}

# Run test
Write-Host "Invoking Tests";
Write-Host;

$configuration = @{
    Run = @{
        Path = "$PSScriptRoot\..\Tests"
    }
    Output = @{
        Verbosity = 'Detailed'
    }
    Should = @{
        ErrorAction = 'Stop'
    }
    CodeCoverage = @{
        Enable = $false
    }
}

Invoke-Pester `
    -Configuration $configuration;

$ProgressPreference = $OriginalPref