. "$PSScriptRoot/Private/Constants.ps1";
. "$PSScriptRoot/Private/GetUrls.ps1";
. "$PSScriptRoot/Private/GetInstanceGuidsForWebApp.ps1";
. "$PSScriptRoot/Private/MapUrlsWithHostName.ps1";

function Start-WakeUpAzureWebApp{

    <#
        .SYNOPSIS
            Wake up Azure WebApp for the current subsribtion with name $TargetWebAppName
        .DESCRIPTION
            Use a list of $WarmupUrls to warm up the specified Azure WebApp for the current subsribtion with name $TargetWebAppName
            Targeting all instances one by one to make sure every instance is warmed up
        .PARAMETER TargetWebAppName
            The actual name of the Azure WebApp
        .PARAMETER WarmupUrls
            A list of string containing relative urls
            Absolute urls are allowed, this will ignore the webapp hostname and use the absolute hostname
            This will only work for "http(s)://"
        .PARAMETER RequestTimeOut
            This parameter requires System.TimeSpan. it defines the maximum amount of time allowed for every web request to respond within.  
            When defined this will terminate script on timeout.
            (Default $null)
        .PARAMETER NoResponseRetries
            This parameter requires a positive integer larger than 0, it defines the amount of times to retry on a non-status code response or timeout.
            (Default 3)
        .PARAMETER ConsecutiveErrorThreshold
            This parameter requires a positive integer larger than 0,  
            it defines the amount of request errors that are allowed to appear in series before terminating.  
            (Default 10)
        .PARAMETER TotalErrorThreshold
            This parameter requires a positive integer larger than 0 but is optional,  
            it defines the amount of request errors that are allowed to appear in total before terminating.  
            (Default no maximum)
        .PARAMETER UseExitInsteadOfThrow
            (Flag) If this flag is enabled the error won't be trown but exit(1) will be used.
            This makes the process terminate but won't print out the stacktrace.
        .PARAMETER OutputHtml
            (Flag) Output response HTML.
        .OUTPUTS
            Some details about the progress and the response code of the urls
        .NOTES
            This module is designed for use in Azure context, so Start-WakeUpAzureWebApp needs to be called before executing the sript.
            This is designed with Octopus deploy in mind.
        .LINK
            https://gitlab.com/Valtech-Amsterdam/PowerShell/WakeUpAzureWebApp
        .EXAMPLE
            Import-Module WakeUpAzureWebApp;

            $targetWebAppName = #"{WebAppName}";
            $warmupUrls = @(
                "/nl-nl"
                "/nl-nl/some url/"
                "/nl-nl/another url/"
            );

            Start-WakeUpAzureWebApp `
                -targetWebAppName $targetWebAppName `
                -warmupUrls $warmupUrls;
    #>

	[CmdletBinding()] Param(

        [Parameter(Mandatory=$True)]
        [string]$TargetWebAppName,
        [Parameter(Mandatory=$True)]
        [string[]]$WarmupUrls,
        [Parameter(Mandatory=$False)] [AllowNull()]
        [Nullable[System.TimeSpan]]$RequestTimeOut = $Constants.Defaults.RequestTimeOut,
        [Parameter(Mandatory=$False)]
        [Int16]$NoResponseRetries = $Constants.Defaults.NoResponseRetries,
        [Parameter(Mandatory=$False)]
        [int16]$ConsecutiveErrorThreshold = $Constants.Defaults.ConsecutiveErrorThreshold,
        [Parameter(Mandatory=$False)] [AllowNull()]
        [Nullable[int16]]$TotalErrorThreshold = $null,
        [Switch] [AllowNull()]
        [bool]$UseExitInsteadOfThrow,
        [Switch] [AllowNull()]
        [bool]$OutputHtml
    )

	BEGIN {
        $scriptStart = [System.DateTime]::UtcNow;

        if ([string]::IsNullOrEmpty($(Get-AzContext).Account)) {
            Write-Host "Please login using Connect-AzAccount" -ForegroundColor Red;
            exit 1;
        }

        try {
            $webApp = Get-AzWebApp -Name $TargetWebAppName -ErrorAction Stop;

            $defaultHostName = $webApp.DefaultHostName;
            $instanceGuids = GetInstanceGuidsForWebApp -webApp $webApp;
            $domainUrls = MapUrlsWithHostName -defaultHostName $defaultHostName -urls $WarmupUrls;
            $instanceCount = @($instanceGuids).length;
        }
        catch {
            if($UseExitInsteadOfThrow) {
                Write-Host "A critical error occured, reason: $_" -ForegroundColor Red
                exit 1;
            }
            else {
                throw;
            }
        }

    }

    PROCESS {

        Write-Host "Getting urls for $instanceCount instances"
        for($i = 0; $i -lt $instanceCount; $i++){
            $instanceGuid = @($instanceGuids)[$i];

            $index = $i + 1;
            Write-Host "Warming up instance $instanceGuid ($index / $instanceCount)";
            Write-Progress -Id 91 -Activity "Getting urls for $instanceCount instances" `
                -status "Warming up instance $instanceGuid $index / $instanceCount)" `
                -percentComplete (($i / $instanceCount) * 100);

            try {
                GetUrls `
                    -defaultHostName $defaultHostName `
                    -urls $domainUrls `
                    -instanceGuid $instanceGuid `
                    -requestTimeOut $RequestTimeOut `
                    -consecutiveErrorThreshold $ConsecutiveErrorThreshold `
                    -totalErrorThreshold $TotalErrorThreshold `
                    -outputHtml $OutputHtml;
            }
            catch {
                if($UseExitInsteadOfThrow) {
                    Write-Host "A critical error occured, reason: $_" -ForegroundColor Red
                    exit 1;
                }
                else {
                    throw;
                }
            }
        }

    }

    End {

        $scriptTimeSpan = New-TimeSpan -Start $scriptStart -End ([System.DateTime]::UtcNow);
        $timeString = $scriptTimeSpan.ToString($Constants.TimeSpanFormat);
        Write-Host "Completed warming up $instanceCount instances, T: $timeString";
        Write-Progress -Id 91 -Activity "Getting urls for $instanceCount instances" `
            -status "Completed warming up $instanceCount instances, T: $timeString" `
            -percentComplete 100 `
            -Completed;

    }
}

Export-ModuleMember -Function Start-WakeUpAzureWebApp
