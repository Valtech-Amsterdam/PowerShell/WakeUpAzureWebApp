# Changelog  
  
## Release 1.3.1 (Latest)  
  
* Added http support
  
## Release 1.3.0  
  
* Added option for absolute url's
  
## Release 1.2.2  
  
* Added flag to switch throw Error and exit(1)
* Fixed error output
* Added html output flag
  
## Release 1.2.1  
  
* Added flag for non-core powershell to allow webrequests without IE
  
## Release 1.2.0  
  
* Added check to avoid infinite TimeOut  
* TimeOut no longer stops script  
* Unit test all Private functions  
* Added retry policy for non-responsecode responses and TimeOut  
  
## Release 1.1.0  
  
* Added configuration for maximum of errors during import  
* Added error list for immediate faillure  
* Made timeout configurable  
* Added time tagging  
  
## Release 1.0.2
* Made string interpolation consistent

## Release 1.0.1
* Added license
* Fixed powershell package info

## Release 1.0.0 (Initial release, Unpublished)
* Initial release