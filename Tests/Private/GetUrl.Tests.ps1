BeforeAll {
    . (Resolve-Path "$PSScriptRoot/../../Private/GetUrl.ps1");
}

Describe "GetUrls" {
    Mock -CommandName Write-Host -Verifiable -MockWith { };
    Mock -CommandName Write-Output -Verifiable -MockWith { };

    It "GetUrl Returns OK" {

        # Assing
        $instanceGuid ="59d1c5f2-73cd-4eec-98d7-3c595f7e0df6";
        $defaultHostName = "someHostName.tld";
        $url = "https://someHostName.tld/somePage/";
        Mock -CommandName GetHttpRequestWithCookie -Verifiable -MockWith {

            return @{ # [Microsoft.PowerShell.Commands.BasicHtmlWebResponseObject] 
                StatusCode = "200";
                StatusDescription = "OK";
            };
        };

        # Act
        $responseObject = GetUrl -defaultHostName $defaultHostName -url $url -instanceGuid $instanceGuid `
            -requestTimeOut (New-TimeSpan -Seconds 10);

        # Assert
        $responseObject | Should -Not -Be $false;
        
    }

    It "GetUrl NoInstanceGuid Returns OK" {

        # Assing
        $defaultHostName = "someHostName.tld";
        $url = "https://someHostName.tld/somePage/";
        Mock -CommandName GetHttpRequestWithCookie -Verifiable -MockWith {

            return @{ # [Microsoft.PowerShell.Commands.BasicHtmlWebResponseObject] 
                StatusCode = "200";
                StatusDescription = "OK";
            };
        };

        # Act
        $responseObject = GetUrl -defaultHostName $defaultHostName -url $url `
            -requestTimeOut (New-TimeSpan -Seconds 10);

        # Assert
        $responseObject | Should -Not -Be $false;

    }

    It "GetUrl NoInstanceGuid NoParameter Fails" {

        # Assing
        $defaultHostName = "someHostName.tld";
        $url = "https://someHostName.tld/somePage/";

        # Act
        $noUrl = { 
            GetUrl -defaultHostName $defaultHostName -url $null `
                -requestTimeOut (New-TimeSpan -Seconds 10);
        };
        $noDefaultHostName = { 
            GetUrl -defaultHostName $null -url $url `
                -requestTimeOut (New-TimeSpan -Seconds 10);
        };
        $none = { 
            GetUrl -defaultHostName $null -url $null `
                -requestTimeOut (New-TimeSpan -Seconds 10);
        };

        # Assert
        $noUrl | Should -Throw "Cannot bind argument to parameter 'url' because it is an empty string.";
        $noDefaultHostName | Should -Throw "Cannot bind argument to parameter 'defaultHostName' because it is an empty string.";
        $none | Should -Throw "Cannot bind argument to parameter 'defaultHostName' because it is an empty string.";

    }
    
    It "GetNotFoundUrl Returns 404" {

        # Assing
        $instanceGuid ="59d1c5f2-73cd-4eec-98d7-3c595f7e0df6";
        $defaultHostName = "someHostName.tld";
        $url = "https://someHostName.tld/somePage/";
        $webException = [HttpException]::new();
        $webException.Message = "Page not found";
        $webException.ResponseMessage = "Not found";
        $webException.ResponseCode = 404;

        Mock -CommandName GetHttpRequestWithCookie -Verifiable -MockWith {
            throw $webException;
        };

        # Act
        $requestObject = { 
            GetUrl -defaultHostName $defaultHostName -url $url -instanceGuid $instanceGuid `
                -requestTimeOut (New-TimeSpan -Seconds 10);
        }

        # Assert
        $requestObject | Should -Throw $webException.Message;
    }

    It "GetErrorUrl Returns 500" {

        # Assing
        $instanceGuid ="59d1c5f2-73cd-4eec-98d7-3c595f7e0df6";
        $defaultHostName = "someHostName.tld";
        $url = "https://someHostName.tld/somePage/";
        $webException = [HttpException]::new();
        $webException.Message = "An unexpected error occured";
        $webException.ResponseMessage = "Server Error";
        $webException.ResponseCode = 500;

        Mock -CommandName GetHttpRequestWithCookie -Verifiable -MockWith {
            throw $webException;
        };

        # Act
        $requestObject = { 
            GetUrl -defaultHostName $defaultHostName -url $url -instanceGuid $instanceGuid `
                -requestTimeOut (New-TimeSpan -Seconds 10);
        }

        # Assert
        $requestObject | Should -Throw $webException.Message;
    }

    It "GetTimedOutUrl throws exception and terminates" {

        # Assing
        $instanceGuid ="59d1c5f2-73cd-4eec-98d7-3c595f7e0df6";
        $defaultHostName = "someHostName.tld";
        $url = "https://someHostName.tld/somePage/";
        $webException = [HttpException]::new();
        $webException.Message = "An unexpected error occured";
        $webException.ResponseMessage = "Request Timeout";
        $webException.ResponseCode = 408;

        Mock -CommandName GetHttpRequestWithCookie -Verifiable -MockWith {
            throw $webException;
        };

        # Act
        $getUrls = { 
            GetUrl -defaultHostName $defaultHostName -url $url -instanceGuid $instanceGuid `
                -requestTimeOut (New-TimeSpan -Seconds 1);
        };

        # Assert Throws
        $getUrls | Should -Throw $webException.Message;
    }
}