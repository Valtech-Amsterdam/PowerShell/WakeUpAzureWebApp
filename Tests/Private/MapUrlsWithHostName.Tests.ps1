BeforeAll {
    . (Resolve-Path "$PSScriptRoot/../../Private/MapUrlsWithHostName.ps1");

    Mock -CommandName Write-Host -Verifiable -MockWith { };
    Mock -CommandName Write-Output -Verifiable -MockWith { };
}

Describe "MapUrlsWithHostName" {

    It "SingleUrl" {

        # Assing
        $defaultHostName = "someHostName.tld";
        $WarmupUrls = "/page";

        # Act
        $result = MapUrlsWithHostName -defaultHostName $defaultHostName -urls $WarmupUrls;

        # Assert
        $result | Should -Be "https://$defaultHostName/page/";

    }

    It "AbsoluteUrls" {

        # Assing
        $defaultHostName = "someHostName.tld";
        $WarmupUrls = @(
            "http://www.someOtherHostName.tld/InsecureTestPage/"
            "https://www.someOtherHostName.tld"
            "https://www.someOtherHostName.tld/TestPage/"
            "https://www.someOtherHostName.tld/test.png"
        );

        # Act
        $result = MapUrlsWithHostName -defaultHostName $defaultHostName -urls $WarmupUrls;

        # Assert
        $result | Should -Be @(
            "http://www.someOtherHostName.tld/InsecureTestPage"
            "https://www.someOtherHostName.tld"
            "https://www.someOtherHostName.tld/TestPage"
            "https://www.someOtherHostName.tld/test.png"
        );

    }

    It "AbsoluteImageUrl" {

        # Assing
        $defaultHostName = "someHostName.tld";
        $WarmupUrls = "https://www.someOtherHostName.tld/test.png";

        # Act
        $result = MapUrlsWithHostName -defaultHostName $defaultHostName -urls $WarmupUrls;

        # Assert
        $result | Should -Be "https://www.someOtherHostName.tld/test.png";

    }

    It "UrlWithExtension HasNoTrailing /" {

        # Assing
        $defaultHostName = "someHostName.tld";
        $WarmupUrls = "/image.png";

        $WarmupUrls = @(
            "/image.png"
            "/image.png?width=something"
            "/image.png?width=something&height=something"
        );
        # Act
        $result = MapUrlsWithHostName -defaultHostName $defaultHostName -urls $WarmupUrls;

        # Assert
        $result | Should -Be @(
            "https://$defaultHostName/image.png"
            "https://$defaultHostName/image.png?width=something"
            "https://$defaultHostName/image.png?width=something&height=something"
        );

    }

    It "RootUrl HasNoTrailing /" {

        # Assing
        $defaultHostName = "someHostName.tld";
        $WarmupUrls = "/";

        # Act
        $result = MapUrlsWithHostName -defaultHostName $defaultHostName -urls $WarmupUrls;

        # Assert
        $result | Should -Be "https://$defaultHostName"

    }

    It "UrlWithQuery HasNoTrailing /" {

        # Assing
        $defaultHostName = "someHostName.tld";
        $WarmupUrls = @(
            "/test?query=present"
            "/test?flagquery"
        );

        # Act
        $result = MapUrlsWithHostName -defaultHostName $defaultHostName -urls $WarmupUrls;

        # Assert
        $result | Should -Be @(
            "https://$defaultHostName/test?query=present"
            "https://$defaultHostName/test?flagquery"
        );

    }

    It "MultiUrl" {

        # Assing
        $defaultHostName = "someHostName.tld";
        $WarmupUrls = @(
            "/"
            "/page-a/"
            "/page-b"
        );

        # Act
        $result = MapUrlsWithHostName -defaultHostName $defaultHostName -urls $WarmupUrls;

        # Assert
        $result | Should -Be @(
            "https://$defaultHostName"
            "https://$defaultHostName/page-a/"
            "https://$defaultHostName/page-b/"
        );

    }

    It "MissingParameters ShouldFail" {

        # Act
        $noDefaultHostName = { MapUrlsWithHostName -defaultHostName "hostname"; }
        $noUrls = { MapUrlsWithHostName -urls "/"; }
        $none = { MapUrlsWithHostName; }

        # Assert
        $noDefaultHostName | Should -Throw "urls parameter is null";
        $noUrls | Should -Throw "defaultHostName parameter is null or empty";
        $none | Should -Throw "defaultHostName parameter is null or empty";

    }
}