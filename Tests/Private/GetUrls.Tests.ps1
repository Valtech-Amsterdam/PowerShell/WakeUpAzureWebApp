BeforeAll {
    . (Resolve-Path "$PSScriptRoot/../../Private/GetUrls.ps1");

    Mock -CommandName Write-Host -Verifiable -MockWith { };
    Mock -CommandName Write-Output -Verifiable -MockWith { };
}

Describe "GetUrls" {

    It "GetUrl Returns OK" {

        # Assing
        $instanceGuid ="59d1c5f2-73cd-4eec-98d7-3c595f7e0df6";
        $defaultHostName = "someHostName.tld";
        $url = "https://someHostName.tld/somePage/";
        Mock -CommandName GetUrl -Verifiable -MockWith {

            return @{ # [Microsoft.PowerShell.Commands.BasicHtmlWebResponseObject] 
                StatusCode = "200";
                StatusDescription = "OK";
            };
        };

        # Act
        GetUrls -defaultHostName $defaultHostName -urls $url -instanceGuid $instanceGuid `
            -requestTimeOut (New-TimeSpan -Seconds 10) `
            -consecutiveErrorThreshold 10 `
            -totalErrorThreshold $null;

        # Assert
        Assert-MockCalled 'Write-Host' -Exactly 1 -Scope It -ParameterFilter { 
            $Object -eq "  OK (200)" -and $foregroundcolor -eq "Green" 
        }
        
    }

    It "GetUrl NoInstanceGuid Returns OK" {

        # Assing
        $defaultHostName = "someHostName.tld";
        $url = "https://someHostName.tld/somePage/";
        Mock -CommandName GetUrl -Verifiable -MockWith {

            return @{ # [Microsoft.PowerShell.Commands.BasicHtmlWebResponseObject] 
                StatusCode = "200";
                StatusDescription = "OK";
            };
        };

        # Act
        GetUrls -defaultHostName $defaultHostName -urls $url `
            -requestTimeOut (New-TimeSpan -Seconds 10) `
            -consecutiveErrorThreshold 10 `
            -totalErrorThreshold $null;

        # Assert
        Assert-MockCalled 'Write-Host' -Exactly 1 -Scope It -ParameterFilter { 
            $Object -eq "  OK (200)" -and $foregroundcolor -eq "Green" 
        }

    }

    It "GetUrl NoInstanceGuid NoParameter Fails" {

        # Assing
        $defaultHostName = "someHostName.tld";
        $url = "https://someHostName.tld/somePage/";

        # Act
        $noUrl = { 
            GetUrls -defaultHostName $defaultHostName -urls $null `
                -requestTimeOut (New-TimeSpan -Seconds 10) `
                -consecutiveErrorThreshold 10 `
                -totalErrorThreshold $null; 
        };
        $noDefaultHostName = { 
            GetUrls -defaultHostName $null -urls $url `
                -requestTimeOut (New-TimeSpan -Seconds 10) `
                -consecutiveErrorThreshold 10 `
                -totalErrorThreshold $null; 
        };
        $none = { 
            GetUrls -defaultHostName $null -urls $null `
                -requestTimeOut (New-TimeSpan -Seconds 10) `
                -consecutiveErrorThreshold 10 `
                -totalErrorThreshold $null; 
        };

        # Assert
        $noUrl | Should -Throw "Cannot bind argument to parameter 'urls' because it is null.";
        $noDefaultHostName | Should -Throw "Cannot bind argument to parameter 'defaultHostName' because it is an empty string.";
        $none | Should -Throw "Cannot bind argument to parameter 'defaultHostName' because it is an empty string.";

    }
    
    It "GetNotFoundUrl Returns 404" {

        # Assing
        $instanceGuid ="59d1c5f2-73cd-4eec-98d7-3c595f7e0df6";
        $defaultHostName = "someHostName.tld";
        $url = "https://someHostName.tld/somePage/";
        $webException = [HttpException]::new();
        $webException.Message = "Page not found";
        $webException.ResponseMessage = "Not found";
        $webException.ResponseCode = 404;

        Mock -CommandName GetUrl -Verifiable -MockWith {

            throw $webException;
        };

        # Act
        GetUrls -defaultHostName $defaultHostName -urls $url -instanceGuid $instanceGuid `
            -requestTimeOut (New-TimeSpan -Seconds 10) `
            -consecutiveErrorThreshold 10 `
            -totalErrorThreshold $null; 

        # Assert
        Assert-MockCalled 'Write-Host' -Exactly 1 -Scope It -ParameterFilter { 
            $Object -eq "  Not Found (404)" -and $foregroundcolor -eq "Magenta" 
        }
    }

    It "GetErrorUrl Returns 500" {

        # Assing
        $instanceGuid ="59d1c5f2-73cd-4eec-98d7-3c595f7e0df6";
        $defaultHostName = "someHostName.tld";
        $url = "https://someHostName.tld/somePage/";
        $webException = [HttpException]::new();
        $webException.Message = "An unexpected error occured";
        $webException.ResponseMessage = "Server Error";
        $webException.ResponseCode = 500;

        Mock -CommandName GetUrl -Verifiable -MockWith {

            throw $webException;
        };

        # Act
        GetUrls -defaultHostName $defaultHostName -urls $url -instanceGuid $instanceGuid `
            -requestTimeOut (New-TimeSpan -Seconds 10) `
            -consecutiveErrorThreshold 10 `
            -totalErrorThreshold $null; 

        # Assert
        Assert-MockCalled 'Write-Host' -Exactly 1 -Scope It -ParameterFilter { 
            $Object -eq "  Server Error (500)" -and $foregroundcolor -eq "Red" 
        }
    }

    It "GetTimeOutUrl Returns 408" {

        # Assing
        $instanceGuid ="59d1c5f2-73cd-4eec-98d7-3c595f7e0df6";
        $defaultHostName = "someHostName.tld";
        $url = "https://someHostName.tld/somePage/";
        $webException = [HttpException]::new();
        $webException.Message = "An unexpected error occured";
        $webException.ResponseMessage = "Request Timeout";
        $webException.ResponseCode = 408;

        Mock -CommandName GetUrl -Verifiable -MockWith {

            throw $webException;
        };

        # Act
        GetUrls -defaultHostName $defaultHostName -urls $url -instanceGuid $instanceGuid `
            -requestTimeOut (New-TimeSpan -Seconds 10) `
            -consecutiveErrorThreshold 10 `
            -totalErrorThreshold $null; 

        # Assert
        Assert-MockCalled 'Write-Host' -Exactly 1 -Scope It -ParameterFilter { 
            $Object -eq "  Request Timeout (408)" -and $foregroundcolor -eq "Magenta" 
        }
    }

    It "GetNotFoundUrl Returns too many consecutive 500 and terminates" {

        # Assing
        $instanceGuid ="59d1c5f2-73cd-4eec-98d7-3c595f7e0df6";
        $defaultHostName = "someHostName.tld";
        $url = @( 
            "https://someHostName.tld/somePage/";
            "https://someHostName.tld/somePage/";
            "https://someHostName.tld/somePage/";
        )
        $webException = [HttpException]::new();
        $webException.Message = "An unexpected error occured";
        $webException.ResponseMessage = "Server Error";
        $webException.ResponseCode = 500;

        Mock -CommandName GetUrl -Verifiable -MockWith {

            throw $webException;
        };

        # Act
        $getUrls = { 
            GetUrls -defaultHostName $defaultHostName -urls $url -instanceGuid $instanceGuid `
                -requestTimeOut (New-TimeSpan -Seconds 10) `
                -consecutiveErrorThreshold 2 `
                -totalErrorThreshold $null;

            # Assert Scope
            Assert-MockCalled GetUrl -Exactly 3 -Scope It
        };

        # Assert Throws
        $getUrls | Should -Throw "The consecutive error threshold of 2 has been reached, terminating script";
    }

    It "GetNotFoundUrl Returns too many total 500 and terminates" {

        # Assing
        $instanceGuid ="59d1c5f2-73cd-4eec-98d7-3c595f7e0df6";
        $defaultHostName = "someHostName.tld";
        $url = @( 
            "https://someHostName.tld/somePage/";
            "https://someHostName.tld/somePage/";
            "https://someHostName.tld/somePage/";
            "https://someHostName.tld/somePage/";
        )
        $webException = [HttpException]::new();
        $webException.Message = "An unexpected error occured";
        $webException.ResponseMessage = "Server Error";
        $webException.ResponseCode = 500;

        Mock -CommandName GetUrl -Verifiable -MockWith {
            throw $webException;
        };

        # Act
        $getUrls = { 
            GetUrls -defaultHostName $defaultHostName -urls $url -instanceGuid $instanceGuid `
                -requestTimeOut (New-TimeSpan -Seconds 10) `
                -consecutiveErrorThreshold 10 `
                -totalErrorThreshold 2;

            # Assert Scope
            Assert-MockCalled GetUrl -Exactly 2 -Scope It
        };

        # Assert Throws
        $getUrls | Should -Throw "The total error threshold of 2 has been reached, terminating script";
    }
}