BeforeAll {
    . (Resolve-Path "$PSScriptRoot/../../Private/GetInstanceGuidsForWebApp.ps1");
}

function Get-AzResource{ }

Describe "GetInstanceGuidsForWebApp" {
    Mock -CommandName Write-Host -Verifiable -MockWith { };
    Mock -CommandName Write-Output -Verifiable -MockWith { };

    It "LoggedIn Returns List" {

        # Assing
        $expectedIds = @(
            "59d1c5f2-73cd-4eec-98d7-3c595f7e0df6"
            "0d040e5c-9401-4538-bed4-66bd2bd64b75"
        );
        $webApp = @{
            Name = "appname-azure-id";
            ResourceGroup = "appresource-azure-id";
        };
        Mock -CommandName Get-AzResource -Verifiable -MockWith {
            
            $mockData = ($expectedIds | ForEach-Object { 
                return @{ Name = $_; };
            }); 

            return $mockData;
        };

        # Act
        $result = GetInstanceGuidsForWebApp -webApp $webApp;

        # Assert
        $result | Should -Be $expectedIds;

    }

    It "NoWebApp Throws" {

        # Act
        $result = { GetInstanceGuidsForWebApp; }

        # Assert
        $result | Should -Throw "webApp parameter is null";

    }
}