BeforeAll {
    . (Resolve-Path "$PSScriptRoot/../../Private/GetHttpRequestWithCookie.ps1");
}

Describe "GetHttpRequestWithCookie" {
    Mock -CommandName Write-Host -Verifiable -MockWith { };
    Mock -CommandName Write-Output -Verifiable -MockWith { };

    It "GetUrl Returns OK" {

        # Assing
        $instanceGuid ="59d1c5f2-73cd-4eec-98d7-3c595f7e0df6";
        $defaultHostName = "someHostName.tld";
        $url = "https://someHostName.tld/somePage/";
        Mock -CommandName Invoke-WebRequest -Verifiable -MockWith {

            return @{ # [Microsoft.PowerShell.Commands.BasicHtmlWebResponseObject] 
                StatusCode = "200";
                StatusDescription = "OK";
            };
        };

        # Act
        $result = GetHttpRequestWithCookie -defaultHostName $defaultHostName -url $url -instanceGuid $instanceGuid `
            -requestTimeOut (New-TimeSpan -Seconds 10);

        # Assert
        $result.StatusCode | Should -Be "200";

    }

    It "GetUrl NoInstanceGuid Returns OK" {

        # Assing
        $defaultHostName = "someHostName.tld";
        $url = "https://someHostName.tld/somePage/";
        Mock -CommandName Invoke-WebRequest -Verifiable -MockWith {

            return @{ # [Microsoft.PowerShell.Commands.BasicHtmlWebResponseObject] 
                StatusCode = "200";
                StatusDescription = "OK";
            };
        };

        # Act
        $result = GetHttpRequestWithCookie -defaultHostName $defaultHostName -url $url `
            -requestTimeOut (New-TimeSpan -Seconds 10);

        # Assert
        $result.StatusCode | Should -Be "200";

    }

    It "GetUrl NoInstanceGuid NoParameter Fails" {

        # Assing
        $defaultHostName = "someHostName.tld";
        $url = "https://someHostName.tld/somePage/";

        # Act
        $noUrl = { 
            GetHttpRequestWithCookie -defaultHostName $defaultHostName -url $null `
                -requestTimeOut (New-TimeSpan -Seconds 10);
        };
        $noDefaultHostName = { 
            GetHttpRequestWithCookie -defaultHostName $null -url $url `
                -requestTimeOut (New-TimeSpan -Seconds 10);
        };
        $none = { 
            GetHttpRequestWithCookie -defaultHostName $null -url $null `
                -requestTimeOut (New-TimeSpan -Seconds 10);
        };

        # Assert
        $noUrl | Should -Throw "Cannot bind argument to parameter 'url' because it is an empty string.";
        $noDefaultHostName | Should -Throw "Cannot bind argument to parameter 'defaultHostName' because it is an empty string.";
        $none | Should -Throw "Cannot bind argument to parameter 'defaultHostName' because it is an empty string.";

    }

    It "GetUrl RequestTimeOutInvalid Fails" {

        # Assing
        $defaultHostName = "someHostName.tld";
        $url = "https://someHostName.tld/somePage/";

        # Act
        $getRequest = { 
            GetHttpRequestWithCookie -defaultHostName $defaultHostName -url $url `
                -requestTimeOut (New-TimeSpan -Seconds 0);
        };

        # Assert
        $getRequest | Should -Throw "requestTimeOut requires to be at least 1 second or more.";

    }

    It "GetNotFoundUrl Returns 404" {

        # Assing
        $instanceGuid ="59d1c5f2-73cd-4eec-98d7-3c595f7e0df6";
        $defaultHostName = "someHostName.tld";
        $url = "https://someHostName.tld/somePage/";
        $webException = [HttpException]::new();
        $webException.Message = "Page not found";
        $webException.ResponseMessage = "Not found";
        $webException.ResponseCode = 404;

        Mock -CommandName Invoke-WebRequest -Verifiable -MockWith {
            throw $webException;
        };

        # Act
        $getRequest = { 
            GetHttpRequestWithCookie -defaultHostName $defaultHostName -url $url -instanceGuid $instanceGuid `
                -requestTimeOut (New-TimeSpan -Seconds 10);
        };

        # Assert
        try { $getRequest }
        catch{ $_.Message | Should -Be $webException.Message }
    }

    It "GetUrl Returns 500" {

        # Assing
        $instanceGuid ="59d1c5f2-73cd-4eec-98d7-3c595f7e0df6";
        $defaultHostName = "someHostName.tld";
        $url = "https://someHostName.tld/somePage/";
        $webException = [HttpException]::new();
        $webException.Message = "An unexpected error occured";
        $webException.ResponseMessage = "Server Error";
        $webException.ResponseCode = 500;

        Mock -CommandName Invoke-WebRequest -Verifiable -MockWith {
            throw $webException;
        };

        # Act
        $getRequest = { 
            GetHttpRequestWithCookie -defaultHostName $defaultHostName -url $url -instanceGuid $instanceGuid `
                -requestTimeOut (New-TimeSpan -Seconds 10);
        };

        # Assert
        try { $getRequest }
        catch{ $_.Message | Should -Be $webException.Message }
    }

    It "GetTimedOutUrl throws exception" {

        # Assing
        $instanceGuid ="59d1c5f2-73cd-4eec-98d7-3c595f7e0df6";
        $defaultHostName = "someHostName.tld";
        $url = "https://someHostName.tld/somePage/";

        Mock -CommandName Invoke-WebRequest -Verifiable -MockWith {
            throw [System.Exception]::new("The operation has timed out");
        };

        # Act
        $getUrls = { 
            GetHttpRequestWithCookie -defaultHostName $defaultHostName -url $url -instanceGuid $instanceGuid `
                -requestTimeOut (New-TimeSpan -Seconds 1);
        };

        # Assert Throws
        $getUrls | Should -Throw "The operation has timed out";
    }
}