. "$PSScriptRoot/Constants.ps1";

# Add default hostname to a list of relative urls
function  MapUrlsWithHostName([string]$defaultHostName, [string[]]$urls){

    # Assert parameters
    if ([string]::IsNullOrWhiteSpace($defaultHostName)) { throw "defaultHostName parameter is null or empty"; }
    if ($urls -eq $null) { throw "urls parameter is null"; }

    return $urls | Foreach-Object {

        if($_ -match $Constants.AbsoluteUrlPattern) {
            $compositeUrl = $_;
        }
        else{
            # Make sure no trailing slashes come through
            $path = $_.Trim('/');

            # Combine url and path
            $compositeUrl = Join-Path -Path $defaultHostName -ChildPath $_;
            $compositeUrl = "https://$compositeUrl";
        }

        # Fix slashes
        $compositeUrl = $compositeUrl.Replace('\', '/');
        $compositeUrl = $compositeUrl.Trim('/');

        # Make sure path ends with a slash, except if extension or no path
        
        if ([string]::IsNullOrWhiteSpace($path)) {
            return $compositeUrl;
        }
        if ($compositeUrl -match $Constants.ExtensionPattern -or
            $compositeUrl -match $Constants.QueryStringParameterPattern) {
            return $compositeUrl;
        }
        
        return "$compositeUrl/";
    };
}