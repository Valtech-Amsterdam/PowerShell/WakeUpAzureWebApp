
$Constants = @{

    Defaults = @{
        RequestTimeOut = [Nullable[System.TimeSpan]]$null
        ConsecutiveErrorThreshold = 10;
        NoResponseRetries = 3;
    };

    TimeSpanFormat = "hh\:mm\:ss\.fff";
    InvokeWebRequestTimeOutMessage = "The operation has timed out.";
    ExtensionPattern = [regex]'\.[a-zA-Z0-9]{1,4}([\?|\&].+\=.+)*$';
    QueryStringParameterPattern = [regex]'\?.*$';
    AbsoluteUrlPattern = [regex]'^http(s?)\:\/\/';

    FilteredErrorMessages = @(

        # Azure WebApp Stopped or Starting
        "Site Disabled";
        "Site Unavailable from Mini-ARR";
        "Service Unavailable";

        # Dll Faillure
        "HTTP Error 502.5 - Process Failure";
    )

};