. "$PSScriptRoot/Constants.ps1";
. "$PSScriptRoot/Models/HttpException.ps1";
. "$PSScriptRoot/GetHttpRequestWithCookie.ps1";

# Make a get request and retry on timeout or non-status code error
function GetUrl(
    [Parameter(Mandatory=$True)]
    [string]$defaultHostName,
    [Parameter(Mandatory=$True)]
    [string]$url,
    [Parameter(Mandatory=$True)] [AllowNull()]
    [Nullable[System.TimeSpan]]$requestTimeOut,
    [Parameter(Mandatory=$False)]
    [string]$instanceGuid,
    [Parameter(Mandatory=$False)]
    [int16]$tries = 1,
    [Parameter(Mandatory=$False)]
    [Boolean]$outputHtml = $False) {

    try {
        return GetHttpRequestWithCookie -defaultHostName $defaultHostName -url $url -instanceGuid $instanceGuid `
            -requestTimeOut $requestTimeOut -logRequest ($tries -le 1) -outputHtml $outputHtml;
    }
    catch [HttpException] {
        $exception = [HttpException]$_.Exception;
        $responseMessage = $exception.ResponseMessage;
        $responseCode = $exception.ResponseCode;

        foreach($message in $Constants.FilteredErrorMessages){
            if ($responseMessage.StartsWith($message)) {
                throw $responseMessage;
            }
        }
        if ($responseCode -eq -1){ throw; }

        $exceptionMessage = $exception.InnerException.Response;
        if ($responseCode -ne 408){ throw; }
        if ($tries -ge $Constants.Defaults.NoResponseRetries) { throw; }
        return GetUrl -defaultHostName $defaultHostName -url $url -instanceGuid $instanceGuid `
            -requestTimeOut $RequestTimeOut -tries ($tries +1);
    }
}