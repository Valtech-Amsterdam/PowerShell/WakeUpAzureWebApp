# Get all instanceGuids currently active for this $appName
function GetInstanceGuidsForWebApp($webApp) {

    if ($webApp -eq $null) { throw "webApp parameter is null"; }

    $appName = $webApp.Name;
    $appId = $webApp.Id;
    Write-Host "Getting all instance id's for webApp $appName";
    
    $resourceId = "$appId/instances"
    $instances = Get-AzResource -ResourceId $resourceId;

    Write-Host $resourceId
    Write-Host $instances

    return $instances | ForEach-Object { return $_.Name.ToString(); };
}