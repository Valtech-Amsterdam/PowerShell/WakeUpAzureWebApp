. "$PSScriptRoot/Constants.ps1";
. "$PSScriptRoot/Models/HttpException.ps1";
. "$PSScriptRoot/GetUrl.ps1";
. "$PSScriptRoot/GetHttpRequestWithCookie.ps1";

# Make a get request to all $urls while forcing the instance using the ARRAfinity cookie with $instanceGuid
function GetUrls(
    [Parameter(Mandatory=$True)]
    [string]$defaultHostName,
    [Parameter(Mandatory=$True)]
    [string[]]$urls,
    [Parameter(Mandatory=$True)] [AllowNull()]
    [Nullable[System.TimeSpan]]$requestTimeOut,
    [Parameter(Mandatory=$True)]
    [int16]$ConsecutiveErrorThreshold,
    [Parameter(Mandatory=$True)] [AllowNull()]
    [Nullable[int16]]$TotalErrorThreshold,
    [Parameter(Mandatory=$False)]
    [string]$instanceGuid,
    [Parameter(Mandatory=$False)]
    [Boolean]$outputHtml = $False) {

    if ($ConsecutiveErrorThreshold -le 0){
        throw "ConsecutiveErrorThreshold needs to be a positive integer larger than 0";
    }
    if ($TotalErrorThreshold -ne $null -and $TotalErrorThreshold -le 0){
        throw "TotalErrorThreshold needs to be a positive integer larger than 0 or null";
    }

    $urlCount = @($urls).length;
    $consecutiveErrorCount = 0;
    $totalErrorCount = 0;
    Write-Host "Getting $urlCount urls for instance $instanceGuid";
    try{
        for($i = 0; $i -lt $urlCount; $i++){
            $url = $urls[$i];
            $index = $i + 1;
            $requestStart = [System.DateTime]::UtcNow;

            Write-Progress -Id 99 -ParentId 91 -Activity ("Getting $urlCount urls for instance $instanceGuid") `
                -status "Processing $index / $urlCount urls" `
                -percentComplete (($i / $urlCount) * 100);

            try {

                $httpResponse = GetUrl `
                    -defaultHostName $defaultHostName `
                    -url $url `
                    -instanceGuid $instanceGuid `
                    -requestTimeOut $RequestTimeOut `
                    -outputHtml $outputHtml;
                $responseMessage = $httpResponse.StatusDescription;
                $responseCode = $httpResponse.StatusCode;

                if($outputHtml -and [System.String]::IsNullOrEmpty($httpResponse.Content) -eq $false){
                    $responseHtml = $httpResponse.Content.Trim() -replace "`n","" -replace "`r","";
                    Write-Host "  Response: $responseHtml" -foregroundcolor Gray;
                }

                Write-Host "  $responseMessage ($responseCode)" -foregroundcolor "Green" -NoNewline;
                $requestTimeSpan = New-TimeSpan -Start $requestStart -End ([System.DateTime]::UtcNow);
                $timeString = $requestTimeSpan.ToString($Constants.TimeSpanFormat);
                Write-Host " [T: $timeString]";
                $consecutiveErrorCount = 0;
            }
            catch [HttpException] {
                $exception = [HttpException]$_.Exception;
                $responseMessage = $exception.ResponseMessage;
                $responseCode = $exception.ResponseCode;

                foreach($message in $Constants.FilteredErrorMessages){
                    if ($responseMessage.StartsWith($message)) { throw $responseMessage; }
                }
                if($outputHtml -and [System.String]::IsNullOrEmpty($exception.ResponseMessage) -eq $false) {
                    $responseHtml = $exception.ResponseData -replace "`n","" -replace "`r","";
                    Write-Host "  Response: $responseHtml" -foregroundcolor Gray;
                }

                $foregroundColor = "Magenta";

                # Red if 5xx status code
                if([math]::Round($responseCode/100) % 5 -eq 0) {
                    $foregroundColor = "Red"
                    $consecutiveErrorCount++;
                    $totalErrorCount++;

                    if($consecutiveErrorCount -ge $ConsecutiveErrorThreshold) {
                        throw "The consecutive error threshold of $ConsecutiveErrorThreshold has been reached, terminating script";
                    }
                    if($TotalErrorThreshold -ne $null -and $totalErrorCount -ge $TotalErrorThreshold) {
                        throw "The total error threshold of $TotalErrorThreshold has been reached, terminating script";
                    }
                }
                # Special cases
                elseif ($responseCode -eq 408 -or $responseCode -eq -1){
                    $consecutiveErrorCount++;
                    $totalErrorCount++;
                }
                else {
                    $consecutiveErrorCount = 0;
                }

                if($responseCode -eq -1){ Write-Host "  $responseMessage" -foregroundcolor Magenta -NoNewline; }
                else { Write-Host "  $responseMessage ($responseCode)" -foregroundcolor $foregroundColor -NoNewline; }
                $requestTimeSpan = New-TimeSpan -Start $requestStart -End ([System.DateTime]::UtcNow);
                $timeString = $requestTimeSpan.ToString($Constants.TimeSpanFormat);
                Write-Host " [T: $timeString]";
            }
        }
    }
    finally{
        $totalErrorCount = 0;
    }

    Write-Progress -Id 99 -ParentId 91 -Activity ("Getting $urlCount urls for instance $instanceGuid") `
        -status "Completed $urlCount urls" `
        -percentComplete 100 `
        -Completed;
}