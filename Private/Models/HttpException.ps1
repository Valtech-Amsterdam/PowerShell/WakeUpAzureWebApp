class HttpException: System.Exception{
    [System.String] $Message;
    [int] $ResponseCode;
    [System.String] $ResponseMessage;
    [System.String] $ResponseData;
}