import-module "$PSScriptRoot/WakeUpAzureWebApp.psm1" -Force -DisableNameChecking
Set-AzContext -SubscriptionId "fba6c856-c187-4bf0-babf-96d282dd98e6"; # This line can be omitted if you have one subscription

# Example usage file, the webapp used in this example might not be up.

$targetWebAppName = "wakeup-example";
$warmupUrls = @(

  # 200
  "/";
  "someurl/";
  "someotherurl/";

  # 3xx
  "301";
  "302";

  # 4xx
  "not-existing/";
  "forbidden/";

  # 5xx
  "error/";
  "badgateway/";
);

Start-WakeUpAzureWebApp `
  -TargetWebAppName $targetWebAppName `
  -WarmupUrls $warmupUrls; # `
  # -RequestTimeOut (New-TimeSpan -Seconds 1) `
  # -NoResponseRetries 1 `
  # -ConsecutiveErrorThreshold 1 `
  # -TotalErrorThreshold 1 `
  # -UseExitInsteadOfThrow `
  # -OutputHtml;